use std::collections::HashMap;
use std::fs;
use std::process;
use std::time;

type Levels = HashMap<usize, String>;

struct Battery {
    levels: Levels,
    identifier: String,
}

impl Battery {
    fn new(identifier: &str) -> Self {
        let mut levels: HashMap<usize, String> = HashMap::new();

        for i in (1..101).rev() {
            let step = 255 - ((256 / 100) * (100 - i));

            if i >= 50 {
                levels.insert(i, format!("#FFFF{:02X}", step));
            } else {
                levels.insert(i, format!("#FF{:02X}00", step));
            }
        }

        Self {
            identifier: String::from(identifier),
            levels: levels,
        }
    }

    fn capacity(&self) -> u32 {
        let file = format!("/sys/class/power_supply/{}/capacity", self.identifier);

        let capacity = match fs::read_to_string(&file) {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Error reading capacity file: {}", e.to_string());
                return 0;
            }
        };

        let capacity: u32 = match capacity.trim().parse() {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Error converting to number: {}", e.to_string());
                return 0;
            }
        };

        capacity
    }

    fn bspc(&self, colour: &str) {
        let mut cmd = process::Command::new("bspc")
            .args(&["config", "focused_border_color", colour])
            .spawn()
            .unwrap();

        let _status = cmd.wait();
    }
}

#[test]
fn generated_levels_test() {
    let battery = Battery::new("BAT0");
    for i in (1..101).rev() {
        assert_eq!(battery.levels.contains_key(&(i as usize)), true);
    }
}

fn main() {
    let battery = Battery::new("BAT0");

    loop {
        let cap = battery.capacity();

        let colour = battery.levels.get(&(cap as usize)).unwrap();
        battery.bspc(colour);

        std::thread::sleep(time::Duration::from_secs(20))
    }
}
